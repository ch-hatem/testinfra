# Technical Test for Cloud Engineer Position

## Scenario

You have been asked to deploy a WebSocket server. The server implementation here is a helm chart. as a cloud engineer you have to ensure infra structure deployment

## Requirements

Everything as to be deployed in aws. Use a publically available aws account of your own or deploy locally 
A pre filled Helm chart for an echo server is at disposal
Terraform code is also provided to deploy a kubernetes cluster

# Local deployment

To locally deploy your terraform infrastructure you can use localstack.

Install localstack [Link](https://docs.localstack.cloud/getting-started/installation/) 
Use localstack with terraform [Link](https://docs.localstack.cloud/user-guide/integrations/terraform/) 

Or use the [docker provider] (https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs) to directly deploy the webserver locally. In this case dont bother with kubnernetes simply deploy the docker image. You will have to change terraform code accordingly. 


## Tasks

1. Deploy an infrastructure to host the provided websocket server. Either an AWS kubernetes cluster or a local docker infrastructure. Use terraform both ways
2. The provided terraform code is a monolith. Can you propose a way to organize it in amore managable way? (implement it in your delivery) 

## Questions 

1. Can you propose a way (tool or process) to manage our deployment on multiple environment (dev, QA, prod, etc...)
2. Can you propose a way (tool or process) to manage our deployment on multiple provider (GCP + AWS)?

## Deliverables

A GitHub repository containing everything needed to rdeploy your work (either a fork of this one or a branch in this repository)
A README file that includes instructions for using your work and your answers to above questions
A brief write-up detailing the design decisions made during the implementation, including any trade-offs or considerations.

## Time Indication

The test should take no more than 3 hours to complete. Dont' try to do everything be effective. Good luck!
