variable "vpc_name" {
}

variable "vpc_cidr" {
}

variable "vpc_private_subnet" {
}

variable "vpc_public_subnet" {
}

variable "enable_nat_gateway" {

}
variable "single_nat_gateway" {

} 
variable "enable_dns_hostnames" {

} 

variable "cluster_version" {

} 
variable "eks_managed_node_groups" {

} 
 


