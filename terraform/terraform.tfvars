# Vpc variables
vpc_name = "k8s-vpc"
vpc_cidr = "172.16.0.0/16"
vpc_private_subnet      = ["172.16.1.0/24", "172.16.2.0/24", "172.16.3.0/24"]
vpc_public_subnet       = ["172.16.4.0/24", "172.16.5.0/24", "172.16.6.0/24"]
enable_nat_gateway   = true
single_nat_gateway   = true
enable_dns_hostnames = true
# Cluster variables
cluster_version = "1.24"
eks_managed_node_groups = {
    first = {
      desired_capacity = 1
      max_capacity     = 10
      min_capacity     = 1

      instance_type = "m5.large"
    }
  }

